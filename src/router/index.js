import Vue from 'vue';
import Router from 'vue-router';
import pages from '@/pages';

Vue.use(Router);

export default new Router({
  routes: [
    pages.PageNotFound,
    pages.Login,
    pages.About,
    pages.User,
    pages.Fillup,
    pages.Admin
  ],
});
