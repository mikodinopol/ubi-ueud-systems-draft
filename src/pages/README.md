App Pages
====================

This directory contains individual web page components.

## Creating New Page

#### Default Structure

When creating new Vue page you must follow the standards for creating a new page

```
* page-name
|-- PageName.Vue
|-- style.scss
|-- index.js
```

#### Page.Vue

When creating a Vue template please consider the following our standard code structure of the file

```
<template>
  <div id="page-name">
    <!-- Page Content Here -->
  </div>
</template>

<script>
import index from './index'
export default index
</script>

<style lang="scss" scoped>
@import "./style.scss";
</style>
```


#### Page Index

This is the script part of Vue component.

Why is this important? this could help us modularized our code separated to the template and style files.

Because sooner or later your code gets bloated and you don't want to look at a source code containing thousand lines of mixed codes 😵

And it gives you benefits of shortening your import code (eg: import DashboardPage from `./pages/dashboard/Dashboard` to `./pages/dashboard` 🤓)

```
export default {
  name: 'page-name'
}
```


#### Page Styles

By default, we'll be using `scss` as our selected language for the page style. This will allow us to have more flexible CSS code.

You can learn more about SCSS/Sass [here](http://sass-lang.com/guide).


#### Adding Page Route

Once you have created the page, we need to register a path for the page.

Go to `pages/index.js` file and add a route inside the `export default` object

```
import PageName from './page-name' // Import the .vue file first

export default {
    routes: [
        // other routes here...
        PageName: {
            path: '/the-url-path',
            name: PageName.name,
            component: PageName
        }
    ]
}
```
