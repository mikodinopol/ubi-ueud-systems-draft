import { NavGuard } from '@/services/NavigationGuard';

import Fillup from './fillup/Fillup';
import PageNotFound from './page-not-found/PageNotFound';
import About from './about/About';
import Login from './login/Login';
import Admin from './admin/Admin';
import AdminDashboard from './admin/admin-dashboard/AdminDashboard';
import Admins from './admin/admins/Admins';
import Provider from './admin/provider/Provider';
import Equipments from './admin/equipments/Equipments';
import Devices from './admin/devices/Devices';
import Project from './admin/project/Project';
import User from './user/User';
import Report from './user/report/Report';
import Equipment from './user/equipment/Equipment';
import BillingList from './user/billing/BillingList';
import Billing from './user/billing/Billing';
import Dashboard from './user/dashboard/Dashboard';

export default {
  Login: {
    path: '/login',
    name: Login.name,
    beforeEnter: NavGuard.UnAuthenticated,
    component: Login,
  },
  Admin: {
    path: '/admin/',
    name: Admin.name,
    component: Admin,
    beforeEnter: NavGuard.Authenticated,
    children: [
      {
        path: 'admin-dashboard',
        name: AdminDashboard.name,
        component: AdminDashboard,
        icon: 'icon-film',
        meta: { requiresAuth: true, adminAuth: true, userAuth: false },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                next('/dashboard')
              }
            } else if(to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                console.log('Im an admin');
                next('/admin/admin-dashboard')
              }
            }
          } else { next() }
        }
      },
      {
        path: 'admins',
        name: Admins.name,
        component: Admins,
        icon: 'icon-people',
        meta: { requiresAuth: true, adminAuth: true, userAuth: false },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                next('/dashboard')
              }
            } else if(to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                console.log('Im an admin');
                next('/admin/admins')
              }
            }
          } else { next() }
        }
      },
      {
        path: 'provider',
        name: Provider.name,
        component: Provider,
        icon: 'icon-notebook',
        meta: { requiresAuth: true, adminAuth: true, userAuth: false },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                next('/dashboard')
              }
            } else if(to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                console.log('Im an admin');
                next('/admin/provider')
              }
            }
          } else { next() }
        }
      },
      {
        path: 'equipments',
        name: Equipments.name,
        component: Equipments,
        icon: 'icon-rocket',
        meta: { requiresAuth: true, adminAuth: true, userAuth: false },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                next('/dashboard')
              }
            } else if(to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                console.log('Im an admin');
                next('/admin/equipments')
              }
            }
          } else { next() }
        }
      },
      {
        path: 'devices',
        name: Devices.name,
        component: Devices,
        icon: 'icon-screen-tablet',
        meta: { requiresAuth: true, adminAuth: true, userAuth: false },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                next('/dashboard')
              }
            } else if(to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                console.log('Im an admin');
                next('/admin/devices')
              }
            }
          } else { next() }
        }
      },
      {
        path: 'project',
        name: Project.name,
        component: Project,
        icon: 'icon-layers',
        meta: { requiresAuth: true, adminAuth: true, userAuth: false },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                next('/dashboard')
              }
            } else if(to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                console.log('Im an admin');
                next('/admin/project')
              }
            }
          } else { next() }
        }
      }
    ],
  },
  User: {
    path: '/',
    name: User.name,
    component: User,
    beforeEnter: NavGuard.Authenticated,
    children: [
      {
        path: 'dashboard',
        name: Dashboard.name,
        component: Dashboard,
        icon: 'icon-film',
        meta: { requiresAuth: true, adminAuth: false, userAuth: true },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                next('/admin/admin-dashboard')
              }
            } else if(to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                console.log('Im in user');
                next('/dashboard')
              }
            }
          } else { next() }
        }
      },
      {
        path: 'report',
        name: Report.name,
        component: Report,
        icon: 'icon-pie-chart',
        meta: { requiresAuth: true, adminAuth: false, userAuth: true },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                next('/admin/admin-dashboard')
              }
            } else if(to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                console.log('Im in user');
                next('/report')
              }
            }
          } else { next() }
        }
      },
      {
        path: 'equipment',
        name: Equipment.name,
        component: Equipment,
        icon: 'icon-speedometer',
        meta: { requiresAuth: true, adminAuth: false, userAuth: true },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                next('/admin/admin-dashboard')
              }
            } else if(to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                console.log('Im in user');
                next('/equipment')
              }
            }
          } else { next() }
        }
      },
      {
        path: 'billing',
        name: Billing.name,
        component: Billing,
        icon: 'icon-calculator',
        meta: { requiresAuth: true, adminAuth: false, userAuth: true },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                next('/admin/admin-dashboard')
              }
            } else if(to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                console.log('Im in user');
                next('/billing')
              }
            }
          } else { next() }
        }
      },
      {
        path: 'billing-list',
        name: BillingList.name,
        alias: 'Billing List',
        component: BillingList,
        icon: 'icon-event',
        meta: { requiresAuth: true, adminAuth: false, userAuth: true },
        beforeEnter: (to, from, next) => {
          if(to.meta.requiresAuth) {
            if (to.meta.userAuth) {
              if (localStorage.ueud_user_type === 'user') {
                next()
              } else {
                next('/admin/admin-dashboard')
              }
            } else if(to.meta.adminAuth) {
              if (localStorage.ueud_user_type === 'admin') {
                next()
              } else {
                console.log('Im in user');
                next('/billing-list')
              }
            }
          } else { next() }
        }
      },
    ],
  },
  About: { path: '/about', name: About.name, component: About },
  PageNotFound: { path: '*', name: PageNotFound.name, component: PageNotFound },
  Fillup: { path: '/fillup', name: Fillup.name, component: Fillup},

};
