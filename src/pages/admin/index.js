import pages from '@/pages';
import { Storage } from '@/services/Storage';

export const STORAGE_KEY = {
  TOKEN: 'token',
};

export default {
  name: 'admin',
  data() {
    return {
      drawer: null,
      willShowActionButtons: false,
      menuItems: pages.Admin.children.filter(i => i.icon),

      // Report
      isTableView: false,
    };
  },
  methods: {
    logout() {
      this.$store.commit('user/logout');
      this.$router.push({ name: pages.Login.name });
      Storage.remove(STORAGE_KEY.TOKEN);
      Storage.set('signout', 1);
      this.signOut();
      this.$cookie.set('lname', '');
      this.$cookie.set('fname', '');
      this.$cookie.set('uemail', '');
      this.$cookie.set('gid', '');
    },
    signOut() {
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        console.log('User signed out.');
      });
    },
    showActionItems() {
      this.willShowActionButtons = true;
    },
    hideActionItems() {
      this.willShowActionButtons = false;
    },
    switchReportTableView(isTableView) {
      this.isTableView = isTableView;
    },
  }
};
