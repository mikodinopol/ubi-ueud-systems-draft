import DatePicker from '@/components/input/DatePicker.vue';

import BillingList from './BillingList.vue';
import { Storage } from '@/services/Storage';
import * as Auth from '@/services/Auth';

export default {
  name: 'billing',
  components: {
    DatePicker,
  },
  data() {
    return {
      searchFilter: '',
      currentStep: 1,
      fromDate: '',
      toDate: '',
      selectedUtils: [],
      selected: [],
      selectedBilling: {},
      creatingBill: false,
      snackBar: false,
      daily_util_list: [],
      dialog: false,
      approved_by: '',
      verified_by: ''
    };
  },
  methods: {
    fetchBillableCustomers() {
      const p = {
        date_start: this.fromDate,
        date_end: this.toDate,
      };

      this.$store
          .dispatch('billing/fetchBillableCustomers', p)
          .then(() => {
            this.currentStep++;
          });
    },
    fetchBillableUtils(billing) {
      this.selectedBilling = billing;

      const p = {
        customer_id: billing.customer_id,
        date_start: this.fromDate,
        date_end: this.toDate,
      };

      this.$store
          .dispatch('billing/fetchBillableUtils', p)
          .then(() => {
            this.currentStep++;
            this.fetchBillableAllUtils(billing);
          });
    },
    fetchBillableAllUtils(billing) {
      this.selectedBilling = billing;

      const p = {
        customer_id: billing.customer_id,
        date_start: this.fromDate,
        date_end: this.toDate,
      };

      this.$store
          .dispatch('billing/fetchBillableAllUtils', p)
          .then(response => {
            this.daily_util_list = response.data;
          });
    },
    equipmentName(eqpID) {
      let result = this.$store.getters['equipment/getEquipmentDetails'](eqpID);
      return result[0].eqp_type_name || '';
    },
    gotoBillingList() {
      this.$router.push({ name: BillingList.name });
    },
    addBillingVerification() {
      this.dialog = true;
//      this.createBilling();
    },
    createBilling() {

      if (this.selected.length > 0) {
      const p = {
        customer_id: this.selectedBilling.customer_id,
        billing_date_from: this.fromDate,
        billing_date_to: this.toDate,
        billing_date: this.dtToday(),
        billing_prepared_by: this.preparedByUser,
        billing_notes: '',
        verified_by: this.verified_by,
        approved_by: this.approved_by
      };

      this.creatingBill = true;
      this.snackBar = true;
      this.$store
          .dispatch('billing/createBilling', p)
          .then(response => {
            const data = response.data;
            if (data.length > 0 ) {
              const billing_id = response.data[0].billing_id;
              const billing_items = this.getSelectedEquipment(this.selected);
              // get billing daily util id
              const billable_utils = this.getSelectectedBillableDailyUtil(billing_items);
              this.insertBillingItems(billing_id, billable_utils);
            }
          })
          .finally(() => {
            this.snackBar = false;
            this.createBilling = false;
          });
        } else {
          alert("Please select at least one billing equipment!");
        }
    },
    insertBillingItems(billing_id, billable_utils) {
      const p = {
        eqp_util_id: billable_utils.substring(1),
        billing_id: billing_id,
        billed: true
      };

      this.creatingBill = true;
      this.snackBar = true;
      this.$store
          .dispatch('billing/createBillingItems', p)
          .then(response => {
              const data = response.data;
              this.currentStep++;
              this.dialog = false;
          })
          .finally(() => {
            this.snackBar = false;
            this.createBilling = false;
          });
    },
    getSelectectedBillableDailyUtil(billable_eqp) {
        const eqp = billable_eqp.split(',');
        let selected_eqp = '';
        for (let i = 0; i < eqp.length; i++) {
          for (let c = 0; c < this.daily_util_list.length; c++) {
            if (eqp[i] == this.daily_util_list[c].eqp_id) {
              selected_eqp += `,${this.daily_util_list[c].id}`;
            }
          }
        }
        return selected_eqp; 
    },
    getSelectedEquipment(selected) {
      let eqp = '';

      this.selected.forEach((util) => {
        if (!eqp) {
          eqp += `${util.eqp_id}`;
        } else {
          eqp += `,${util.eqp_id}`;
        }
      });

      return eqp;
    },
    dtToday() {
      const today = new Date();
      const y = today.getFullYear();
      let m = today.getMonth() + 1;
      let d = today.getDate();

      m = (m < 10) ? `0${m}` : m;
      d = (d < 10) ? `0${d}` : d;

      return `${y}-${m}-${d}`;
    },
    printBillingReport() {
      /*
      var printContents = document.getElementById('printable').innerHTML;
      var originalContents = document.body.innerHTML;
 
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
      */

      var divToPrint=document.getElementById('printable');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();
    }
  },
  computed: {
    user() {
      return JSON.parse(localStorage.getItem('user'));
    },
    billableCustomers() {
      return this.$store.getters['billing/getBillableCustomers'];
    },
    billableUtils() {
      return this.$store.getters['billing/getBillableUtilsTable'];
    },
    preparedByUser() {
      return this.$store.getters['user/getFullName'];
    },
    steps() {
      let result = [];

      for (let i = 1; i <= 4; ++i) {
        result.push((i <= this.currentStep) ? true : false);
      }

      return result;
    },
    selectedEquipments() {
      let eqp = '';

      this.selectedUtils.forEach((util) => {
        if (!eqp) {
          eqp += `${util.eqp_id}`;
        } else {
          eqp += `, ${util.eqp_id}`;
        }
      });

      return eqp;
    },
    totalBill() {
      let sum = 0;

      this.selected.forEach((util) => {
        sum += (util.billing_amount) ? Number(util.billing_amount) : 0;
      });
      return sum;

    },
  },
};
