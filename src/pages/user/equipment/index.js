import EquipmentList from '@/components/list/EquipmentList';

export default {
  name: 'equipment',
  components: { EquipmentList },
  data() {
    return {
      searchFilter: '',
      dialog: false,
      selectedType: {},
    };
  },
  methods: {
    selectEquipmentType(equipmentType) {
      this.selectedType = Object.assign({}, equipmentType);
      this.dialog = true;
    },
  },
  watch: {
    dialog(newVal) {
      if (!newVal) {
        setTimeout(() => {
          this.selectedType = Object.assign({}, {});
        }, 200);
      }
    },
  },
  computed: {
    equipmentTypes() {
      return this.$store
                 .getters['equipment/getGroupType'](this.searchFilter);
    },
    equipmentTypesGroup() {
      return this.$store
                .getters['equipment/getGroupDetails'](this.selectedType.id);
    },
  },
};
