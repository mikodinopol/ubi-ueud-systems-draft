import ReportChart from './ReportChart.vue';
import ReportTable from './ReportTable.vue';

export default {
  name: 'report',
  components: { ReportChart, ReportTable },
  mounted() {
    this.$parent.showActionItems();
  },
  destroyed() {
    this.$parent.hideActionItems();
  },
};
