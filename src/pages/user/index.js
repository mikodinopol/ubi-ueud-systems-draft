import pages from '@/pages';
import { Storage } from '@/services/Storage';

export const STORAGE_KEY = {
  TOKEN: 'token',
};

export default {
  name: 'user',
  data() {
    return {
      drawer: null,
      willShowActionButtons: false,
      menuItems: pages.User.children.filter(i => i.icon),
      // Report
      isTableView: false,
    };
  },
  beforeCreate() {
    this.$store.dispatch('equipment/fetchDashboardChart');
    this.$store.dispatch('equipment/fetchEquipmentBrand');
    this.$store.dispatch('equipment/fetchEquipmentType');
    this.$store.dispatch('equipment/fetchEquipmentModel');
    this.$store.dispatch('equipment/fetchEquipment');
    this.$store.dispatch('report/fetchReports');
    this.$store.dispatch('user/fetchUser');
  },
  methods: {
    logout() {
      this.$store.commit('user/logout');
      this.$router.push({ name: pages.Login.name });
      Storage.remove(STORAGE_KEY.TOKEN);
      Storage.set('signout', 1);
      this.signOut();
      this.$cookie.set('lname', '');
      this.$cookie.set('fname', '');
      this.$cookie.set('uemail', '');
      this.$cookie.set('gid', '');
    },
    signOut() {
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        console.log('User signed out.');
      });
    },
    showActionItems() {
      this.willShowActionButtons = true;
    },
    hideActionItems() {
      this.willShowActionButtons = false;
    },
    switchReportTableView(isTableView) {
      this.isTableView = isTableView;
    },
  },
};
