import EquipmentList from '@/components/list/EquipmentList';
import BarChart from '@/components/chart/bar-chart';

export default {
  name: 'dashboard',
  components: {
    EquipmentList,
    BarChart
  },
  data() {
    return {
      current_week: '',
      monthNames: ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
      ],
      chartOptions: {
        scales: {
          yAxes: [{
            gridLines: {
              display: false,
            },
            ticks: {
              beginAtZero: true,
            },
          }],
          xAxes: [{
            gridLines: {
              display: false,
            },
          }],
        },
      },
    }
  },
  methods: {
    getDate(week_date) {
      const date = new Date(parseInt(week_date));
      const month = date.getMonth();
      const year  = date.getFullYear();
      const report_day = date.getDay();

   //   return date.toLocaleDateString();
      return this.monthNames[month] + ' ' + report_day + ', ' + year;
    },
  },
  computed: {
    weekComparisonData() {
      return {
        labels: ['Utility Hrs', 'Idle Hrs', 'Liters'],
        datasets: [
          {
            label: 'Total',
            backgroundColor: ['#10C4D3', 'red', 'green'],
            data: [
              this.currentTotalUtilityHours,
              this.currentTotalIdleHours,
              this.currentTotalLiterHours,
            ],
          }
        ]
    }
    },
    equipments() {
      return this.$store.getters['equipment/getEquipment'];
    },
    currentTotalUtilityHours() {
      return this.$store.getters['equipment/getTotalCurrentUtils'];
    },
    currentTotalIdleHours() {
      return this.$store.getters['equipment/getTotalCurrentIdle'];
    },
    currentTotalLiterHours() {
      return this.$store.getters['equipment/getTotalCurrentLiters'];
    },
   
  },
  created: function() {
    var curr = new Date;
    var first = curr.getDate() - curr.getDay();
    var last = first + 6;

    var monday = new Date(curr.setDate(first)).toDateString();
    var sunday = new Date(curr.setDate(last)).toDateString();
    //alert(monday+" "+sunday)
    this.current_week = monday + '  - ' + sunday;

   // this.current_week = this.getDate(monday.getT)+ ' - ' + this.getDate(sunday);
    /*
    const curr = new Date;
    const firstday = new Date(curr.setDate(curr.getDate() - curr.getDay())).getTime();
    const lastday = new Date(curr.setDate(curr.getDate() - curr.getDay()+6)).getTime();

    
   // this.current_week = firstday +  ' - ' + lastday; */

  } 
};
