import User from './user';
import Equipment from './equipment';
import Billing from './billing';
import Report from './report';

export default [
  { name: 'user', store: User },
  { name: 'equipment', store: Equipment },
  { name: 'billing', store: Billing },
  { name: 'report', store: Report },
];
