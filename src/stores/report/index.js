import axios from '@/utils/Axios';
import PayloadUtil from '@/utils/Payload';
import Colors from '@/utils/Colors';
import DateUtil from '@/utils/DateUtil';

import ReportModel from './report.model';


function sanitizeData(data) {
  return data.map((d) => {
              const dt = Number(d.eqp_datetime.match(/\d/g).join(''));
              const dd = Number(d.ddate.match(/\d/g).join(''));

              d.eqp_datetime = new Date(dt);
              d.ddate = new Date(dd);

              return d;
            });
}


// ------------------------
// Mutations
// ------------------------

const mutations = {
  set(state, reports) {
    state.reports = reports;
  },
};

// ------------------------
// Getters
// ------------------------

const getters = {
  getChartData(state) {
    // Generate gradient fill
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const utilityGradient = ctx.createLinearGradient(0, 0, 0, 400);
          utilityGradient.addColorStop(0, 'rgba(255, 177, 1, 0.9)');
          utilityGradient.addColorStop(1, 'rgba(255, 177, 1, 0)');
    const idleGradient = ctx.createLinearGradient(0, 0, 0, 400);
          idleGradient.addColorStop(0, 'rgba(164, 175, 180, 0.9)');
          idleGradient.addColorStop(1, 'rgba(164, 175, 180, 0)');
    const litersGradient = ctx.createLinearGradient(0, 0, 0, 400);
          litersGradient.addColorStop(0, 'rgba(16, 196, 221, 0.9)');
          litersGradient.addColorStop(1, 'rgba(16, 196, 221, 0)');
    const result = {
      totalFuelLiters: 0,
      lineChart: {
        labels: [
          'Jan', 'Feb', 'Mar', 'Apr',
          'May', 'Jun', 'Jul', 'Aug',
          'Sept', 'Oct', 'Nov', 'Dec',
        ],
        datasets: [
          {
            label: 'Utility',
            backgroundColor: utilityGradient,
            pointBorderColor: '#FFFFFF',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            fill: true,
          },
          {
            label: 'Idle Hrs',
            backgroundColor: idleGradient,
            pointBorderColor: '#FFFFFF',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            fill: true,
          },
          {
            label: 'Liters',
            backgroundColor: litersGradient,
            pointBorderColor: '#FFFFFF',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            fill: true,
          },
        ],
      },
      doughnutChart: {
        labels: ['Utility Hrs', 'Idle Hrs'],
        datasets: [
          {
            backgroundColor: ['#FFDB01', '#A4AFB4'],
            data: [0, 0],
          },
        ],
      },
    };

    let dfuel  = 0;
    let dutil_hours = 0;
    let didle_hours = 0;
    
    let lfuel  = 0;
    let lutil_hours = 0;
    let lidle_hours = 0;

    state.reports.forEach((report) => {
      
      dfuel   +=  parseFloat(report.eqp_fuel_liters);
      dutil_hours  +=  parseFloat(report.eqp_util_hrs);
      didle_hours +=   parseFloat(report.eqp_idle_hrs);

      result.totalFuelLiters  =  parseFloat(dfuel).toFixed(2);
      result.doughnutChart.datasets[0].data[0] = parseFloat(dutil_hours).toFixed(2);
      result.doughnutChart.datasets[0].data[1]  =  parseFloat(didle_hours).toFixed(2);

      const idx = report.eqp_datetime.getMonth();

      
      lfuel  +=  parseFloat(report.eqp_fuel_liters);
      lutil_hours +=  parseFloat(report.eqp_util_hrs);
      lidle_hours +=   parseFloat(report.eqp_idle_hrs);
      
      result.lineChart.datasets[0].data[idx] =  parseFloat(lutil_hours).toFixed(2);
      result.lineChart.datasets[1].data[idx] =  parseFloat(lidle_hours).toFixed(2);
      result.lineChart.datasets[2].data[idx] =  parseFloat(lfuel).toFixed(2);

    

    });

    return result;
  },
};

// ------------------------
// Actions
// ------------------------

const actions = {
  fetchReports(context, payload) {
    return new Promise((resolve, reject) => {
      if (!payload) {
        payload = {
          date_start: '2018-01-01',
          date_end: '2018-12-31',
        };
      }

      axios.get(`/eqpGetReport?${PayloadUtil.toURLParams(payload)}`)
          .then((response) => {
            let data = sanitizeData(response.data);

            context.commit('set', data);

            resolve(data);
          })
          .catch((err) => {
            reject(err);
          });
    });
  },
  fetchEqpReport(context, payload) {
    return new Promise((resolve, reject) => {
      payload.date_start = '2018-01-01';
      payload.date_end = DateUtil.getDateTodayReport();

      let promises = [];

      promises.push(new Promise((res, rej) => {
        axios.get(`/eqpReportGroupByDay?${PayloadUtil.toURLParams(payload)}`)
            .then((response) => {
              res(response.data);
            })
            .catch((err) => {
              rej(err);
            });
      }));
      promises.push(new Promise((res, rej) => {
        axios.get(`/eqpReportGroupByMonth?${PayloadUtil.toURLParams(payload)}`)
            .then((response) => {
              res(response.data);
            })
            .catch((err) => {
              rej(err);
            });
      }));

      Promise
        .all(promises)
        .then((res) => {
          let result = {
            day: {
              labels:  [],
              datasets: [],
            },
            month: {
              labels: ['Utility Hrs', 'Idle Hrs', 'Liters'],
              datasets: [],
            },
            day_sets: {
              datasets: [],
            },
            month_sets: {
              datasets: [],
            }
          };

          
          let label = [];
          let util_hrs = [];
          let idle_hrs = [];
          let fuel_liters = [];
          res[0].forEach((d) => {
            const year = new Date().getFullYear();
            const date = new Date(year, 0);
            label.push(DateUtil.dayFormat(new Date(date.setDate(d.day_of_year))));
            util_hrs.push(d.util_hrs);
            idle_hrs.push(d.idle_hrs);
            fuel_liters.push(d.fuel_liters);
          });
          let array_data = [util_hrs, idle_hrs, fuel_liters]; 
          let array_label = ['Utilize Hrs', 'Idle Hrs', 'Liters'];
          let array_colors = ['#34ff34', '#ff0000', '#1366DE'];
          let i = 0;
          result.day.labels = label;
          array_data.forEach((data) => {
            result.day.datasets.push({
              label : array_label[i],
              backgroundColor: array_colors[i],
              data : data
            })
            i++;
          });
          /*  month graph */
          let mlabel = [];
          let mutil_hrs = [];
          let midle_hrs = [];
          let mfuel_liters = [];

          res[1].forEach((d) => {
            mlabel.push(DateUtil.getMonth(d.month_of_year));
            mutil_hrs.push(d.util_hrs);
            midle_hrs.push(d.idle_hrs);
            mfuel_liters.push(d.fuel_liters);
          });
          array_data =  [mutil_hrs, midle_hrs, mfuel_liters]; 
          array_label = ['Utilize Hrs', 'Idle Hrs', 'Liters'];
          array_colors =['#34ff34', '#ff0000', '#1366DE'];
          i = 0;
          result.month.labels = mlabel;
          array_data.forEach((data) => {
            result.month.datasets.push({
              label : array_label[i],
              backgroundColor: array_colors[i],
              data : data
            })
            i++;
          });
          /* end of month graph */
          result.day_sets = res[0];
          result.month_sets = res[1];
         
          resolve(result);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  fetchReportGroupByDay(context, payload) {
    return new Promise((resolve, reject) => {
      axios.get(`/eqpReportGroupByDay?${PayloadUtil.toURLParams(payload)}`)
          .then((response) => {
            resolve(response.data);
          })
          .catch((err) => {
            reject(err);
          });
    });
  },
  // fetchReportGroupByMonth(context, payload) {
  //   return new Promise((resolve, reject) => {
  //     axios.get('/eqpReportGroupByMonth')
  //         .then((response) => {
  //           resolve(response.data)
  //         })
  //         .catch((err) => {
  //           reject(err);
  //         });
  //   });
  // },
};

// ------------------------
// Store
// ------------------------

const ReportStore = {
  namespaced: true,
  state: ReportModel,
  mutations,
  getters,
  actions,
};

export default ReportStore;
