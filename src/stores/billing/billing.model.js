export default {
  billings: [],
  billableCustomers: [],
  billableUtils: [],
  billableEqpUtils: [],
  billableEqpUtilsByBillingID: [],
  billableEqpByBillingID: [],
  billableUtilsTable: {
    headers: [
      {
        'text': 'Equipment',
        'value': 'eqp_id',
        'align': 'left',
        'sortable': false,
      },
      {
        'text': 'Utilized Hours',
        'value': 'eqp_util_hrs',
        'align': 'left',
        'sortable': false,
      },
      {
        'text': 'Billable Amount',
        'value': 'billing_amount',
        'align': 'left',
        'sortable': false,
      },
    ],
    items: [],
  },
};
