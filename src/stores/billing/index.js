import axios from '@/utils/Axios';
import Payload from '@/utils/Payload';

import BillingModel from './billing.model';


const SD = '2018-01-01';

function dtToday() {
  const today = new Date();
  const y = today.getFullYear();
  let m = today.getMonth() + 1;
  let d = today.getDate();

  m = (m < 10) ? `0${m}` : m;
  d = (d < 10) ? `0${d}` : d;

  return `${y}-${m}-${d + 1}`;
}

// ------------------------
// Mutations
// ------------------------

const mutations = {
  setBillings(state, billings) {
    state.billings = billings;
  },
  setBillableCustomers(state, billableCustomers) {
    state.billableCustomers = billableCustomers;
  },
  setBillableUtils(state, billableUtils) {
    state.billableUtils = billableUtils;
  },
  setBillableEqpUtils(state, billableEqpUtils) {
    state.billableEqpUtils = billableEqpUtils;
  },
  setBillableEqpUtilsByBillingID(state, billableEqpUtilsByBillingID) {
    state.billableEqpUtilsByBillingID = billableEqpUtilsByBillingID;
  },
  setBillableEqpByBillingID(state, billableEqpByBillingID) {
    state.billableEqpByBillingID = billableEqpByBillingID;
  }
};

// ------------------------
// Getters
// ------------------------

const getters = {
  getBillings: (state) => (customer) => {
    return state.billings
                .filter((b) => {
                  return b.customer_name
                          .toUpperCase()
                          .indexOf(customer.toUpperCase() >= 0);
                });
  },
  getBillableCustomers(state) {
    return state.billableCustomers;
  },
  getBillableUtils(state) {
    return state.billableUtils;
  },
  getBillableUtilsTable(state) {
    let table = Object.assign({}, state.billableUtilsTable);

    table.items = state.billableUtils;

    return table;
  },
};

// ------------------------
// Actions
// ------------------------

const actions = {
  fetchBillings(context) {
    axios
      .get(`/eqpGetBilling?date_start=${SD}&date_end=${'2018-12-31'}`)
      .then((response) => {
        context.commit('setBillings', response.data);
      });
  },
  fetchBillableCustomers(context, payload) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/eqpGetBillableCustomers?${Payload.toURLParams(payload)}`)
        .then((response) => {
          context.commit('setBillableCustomers', response.data);
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  fetchBillableUtils(context, payload) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/eqpGetBillableEqp?${Payload.toURLParams(payload)}`)
        .then((response) => {
          context.commit('setBillableUtils', response.data);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  fetchBillableEqpByBillingID(context, payload) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/eqpGetBillableEqpByBillingID?${Payload.toURLParams(payload)}`)
        .then((response) => {
          context.commit('setBillableEqpByBillingID', response.data);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  fetchBillableAllUtils(context, payload) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/eqpGetBillableUtils?${Payload.toURLParams(payload)}`)
        .then((response) => {
          context.commit('setBillableEqpUtils', response.data);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  fetchBillableAllUtilsByBillingID(context, payload) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/eqpGetBillableUtilsByBillingID?${Payload.toURLParams(payload)}`)
        .then((response) => {
          context.commit('setBillableEqpUtilsByBillingID', response.data);
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  createBilling(context, payload) {
    return new Promise((resolve, reject) => {
      payload.billing_date = dtToday();

      axios
        .post('/eqpCreateBilling', payload, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  createBillingItems(context, payload) {
    return new Promise((resolve, reject) => {
  
      axios
        .post('/eqpSetBillingItem', payload, {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};

// ------------------------
// Store
// ------------------------

const BillingStore = {
  namespaced: true,
  state: BillingModel,
  mutations,
  getters,
  actions,
};

export default BillingStore;
