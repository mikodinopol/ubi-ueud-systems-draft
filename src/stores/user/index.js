import axios from '@/utils/Axios';

import { Auth } from '@/services/Auth';
import { Storage } from '@/services/Storage';

import UserModel from './user.model';


const STORAGE_KEY = {
  USER: 'user',
};

// ------------------------
// Mutations
// ------------------------

const mutations = {
  set(state, newUser) {
    state.user = newUser;
    Storage.set(STORAGE_KEY.USER, JSON.stringify(newUser));
  },
  logout(state) {
    state.user = Object.assign({});
    Auth.unAuthenticate();
    Storage.remove(STORAGE_KEY.USER);
    if (localStorage.g_lname !== '' || localStorage.g_fname !== '') { //remove google storage cookies
      localStorage.removeItem('g_lname');
      localStorage.removeItem('g_fname');
      localStorage.removeItem('g_id');
      localStorage.removeItem('g_email');
    } 
      localStorage.removeItem('admin_lname');
      localStorage.removeItem('admin_fname');
      localStorage.removeItem('ueud_user_type');
      localStorage.removeItem('user_img');
  },
};

// ------------------------
// Getters
// ------------------------

const getters = {
  get(state) {
    return state;
  },
  getFullName(state) {
    return `${state.user.user_last}, ${state.user.user_first}`;
  },
  getFirstName(state) {
    return state.user.user_first;
  },
  /*getAdminUsers: (state) => (type) => {
    let result = [];

    state.user_firstname
        .filter((e) => {
          return e.eqp_type_name
                  .toUpperCase()
                  .indexOf(type.toUpperCase()) >= 0;
        })
        .forEach((eqpType) => {
          eqpType.count = state.equipment
                              .filter(e => e.eqp_type_id == eqpType.id).length;

          result.push(Object.assign({}, eqpType));
        });

    return result;
  },*/
};

// ------------------------
// Actions
// ------------------------

const actions = {
  googleLogin(context, payload) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/googleLogin?email=${payload.email}&password=${payload.id_token}`)
        .then((response) => {
          const data = response.data[0];

          if (data.id_token !== 'empty') {
            Auth.authenticate(data.id_token);

            context.commit('set', data);
          } else {
            response['error'] = 'Incorrect Credentials';
          }
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  login(context, payload) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/login?username=${payload.username}&password=${payload.password}`)
        .then((response) => {
          const data = response.data[0];

          if (data.user_token !== 'empty') {
            Auth.authenticate(data.user_token);
          
          } else {
            response['error'] = 'Incorrect Credentials';
          }

          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  fetchUser(context) {
    const user = Storage.get(STORAGE_KEY.USER);

    if (user) {
      context.commit('set', JSON.parse(user));
    }
  },
};

// ------------------------
// Store
// ------------------------

const UserStore = {
  namespaced: true,
  state: UserModel,
  mutations,
  getters,
  actions,
};

export default UserStore;
