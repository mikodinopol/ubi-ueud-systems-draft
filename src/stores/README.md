Vuex Stores
====================

This directory contains Vuex states/stores.

Please read the official documentation https://vuex.vuejs.org/en/getting-started.html to know more about Vuex.

## Usage

By default, you can access Vuex stores in your .vue pages through `this.$store`


## Creating Vuex store

#### Default Structure

When creating new Vuex store you must follow the standards for creating a new store

```
* store-name
|-- index.js
|-- StoreName.model.js
```

Wherein the `store-name` is of course the name of the store/state you created.

#### Register New Store

Upon adding the a new store, you should also register to to Vue.JS so you can now use it on the entire app.

Edit `stores.index.js` and add an entry to `export default`

```
import YourStore from './your-store'

export default [
  // ... other registered stores here
  { name: 'store-name', store: YourStore }
]
```


