import axios from '@/utils/Axios';

import EquipmentModel from './equipment.model';

// ------------------------
// Mutations
// ------------------------

const mutations = {
  setDashboardChart(state, dashboardChart) {
    state.dashboardChart = dashboardChart;
  },
  setEquipment(state, eq) {
    if (eq != null) {
      eq.forEach(function(v, i) {
        if (state.equipment_type) {
          eq[i]['eqp_type_name'] = state.equipment_type
            .find(x => x.id === Number(v.eqp_type_id))
            .eqp_type_name;
        }

        eq[i]['eqp_display_name'] = `${v.eqp_code}-${v.id} (${v.eqp_model_name})`;
      });
    }
    state.equipment = eq;
  },
  setEquipmentTable(state, equipment) {
    let table = Object.assign({ headers: state.equipment_headers, items: [] });

    equipment.forEach(function(val, index) {
      table.items.push({
        eqp_code: val.eqp_code,
        eqp_type_name: val.eqp_type_name,
        eqp_util_hrs: val.eqp_util_hrs,
        eqp_idle_hrs: val.eqp_idle_hrs,
        eqp_liters_per_hour: val.eqp_liters_per_hour,
        eqp_datetime: val.eqp_datetime,
        parsed_date: val.parsed_date,
      });
    });

    state.equipment_table = table;
  },
  setTotalEquipment(state, totalEquipment) {
    state.total_equipment = totalEquipment;
  },
  setEquipmentModel(state, equipmentModel) {
    state.equipment_model = equipmentModel;
  },
  setEquipmentType(state, equipmentType) {
    state.equipment_type = equipmentType;
  },
  setEquipmentBrand(state, equipmentBrand) {
    state.equipment_brand = equipmentBrand;
  },
  setEquipmentDataSet(state, dataset) {
    state.equipment_data_set = dataset;
  },
  setTotalCurrentUtils(state, util) {
    state.total_current_utils = (util <= 0) ? util.toFixed() : util.toFixed(2);
  },
  setTotalCurrentIdle(state, idle) {
    state.total_current_idle = (idle <= 0) ? idle.toFixed() : idle.toFixed(2);
  },
  setTotalCurrentLiters(state, lts) {
    state.total_current_liters = (lts <= 0) ? lts.toFixed() : lts.toFixed(2);
  },
  setTotalPreviousUtils(state, utils) {
    state.total_prev_utils = (utils <= 0) ? utils.toFixed() : utils.toFixed(2);
  },
  setTotalPreviousIdle(state, idle) {
    state.total_prev_idle = (idle <= 0) ? idle.toFixed() : idle.toFixed(2);
  },
  setTotalPreviousLiters(state, lts) {
    state.total_prev_liters = (lts <= 0) ? lts.toFixed() : lts.toFixed(2);
  },
  reset(state) {
    state = Object.assign({}, EquipmentModel);
  },
};

// ------------------------
// Getters
// ------------------------

const getters = {
  getDashboardChart(state) {
    return state.dashboardChart;
  },
  getEquipment(state) {
    return state.equipment;
  },
  getEquipmentTable: (state) => (from, to) => {
    let table = Object.assign({ headers: state.equipment_headers, items: [] });
    let equipments = [];

    if (from && to) {
      const fd = new Date(from);
      const td = new Date(to);

      equipments = state.equipment
                        .filter(e => new Date(e.parsed_date) >= fd
                                  && new Date(e.parsed_date) <= td);
    } else {
      equipments = state.equipment;
    }

    equipments.forEach(function(val, index) {
      table.items.push({
        eqp_code: val.eqp_code,
        eqp_type_name: val.eqp_type_name,
        eqp_util_hrs: val.eqp_util_hrs,
        eqp_idle_hrs: val.eqp_idle_hrs,
        eqp_liters_per_hour: val.eqp_liters_per_hour,
        eqp_datetime: val.eqp_datetime,
        parsed_date: val.parsed_date,
      });
    });

    return table;
  },
  getTotalEquipment(state) {
    return state.total_equipment;
  },
  getEquipmentModel(state) {
    return state.equipment_model;
  },
  getEquipmentType(state) {
    return state.equipment_type;
  },
  getEquipmentBrand(state) {
    return state.equipment_brand;
  },
  getEquipmentDataSet(state) {
    return state.equipment_data_set;
  },
  getTotalCurrentUtils(state) {
    return state.total_current_utils;
  },
  getTotalCurrentIdle(state) {
    return state.total_current_idle;
  },
  getTotalCurrentLiters(state) {
    return state.total_current_liters;
  },
  getTotalPreviousUtils(state) {
    return state.total_prev_utils;
  },
  getTotalPreviousIdle(state) {
    return state.total_prev_idle;
  },
  getTotalPreviousLiters(state) {
    return state.total_prev_liters;
  },
  getEquipmentDetails: (state) => (eqpID) => {
    return state.equipment_type
                .filter(e => e.id === eqpID);
  },
  getGroupType: (state) => (type) => {
    let result = [];

    state.equipment_type
        .filter((e) => {
          return e.eqp_type_name
                  .toUpperCase()
                  .indexOf(type.toUpperCase()) >= 0;
        })
        .forEach((eqpType) => {
          eqpType.count = state.equipment
                              .filter(e => e.eqp_type_id == eqpType.id).length;

          result.push(Object.assign({}, eqpType));
        });

    return result;
  },
  getGroupDetails: (state) => (typeID) => {
    const equipmentType = state.equipment_type.filter(e => e.id === typeID)[0];
    const equipments = state.equipment.filter(e => e.eqp_type_id == typeID);

    let totalUtilityHrs = 0;
    let totalIdleHrs = 0;
    let totalLiters = 0;

    equipments.forEach((eqp) => {
      totalUtilityHrs += eqp.eqp_util_hrs;
      totalIdleHrs += eqp.eqp_idle_hrs;
      totalLiters += eqp.eqp_fuel_liters;
    });

    return {
      equipmentType,
      equipments,
      totalUtilityHrs,
      totalIdleHrs,
      totalLiters,
    };
  },
};

// ------------------------
// Actions
// ------------------------

const actions = {
  fetchEquipment(context) {
    axios.get(`/eqpGetEQP`)
      .then((response) => {
        response.data.map((ele) => {
          const date = new Date(Number(ele.eqp_datetime.match(/\d/g).join('')));
          let m = date.getMonth() + 1;
              m = (m < 10) ? `0${m}` : m;
          let d = date.getDate();
              d = (d < 10) ? `0${d}` : d;

          ele.parsed_date = `${date.getFullYear()}-${m}-${d}`;

          return ele;
        });

        context.commit('setEquipment', response.data);
        context.commit('setEquipmentTable', response.data);
        context.commit('setTotalEquipment', response.data.length);

        let chartDataSet = Object.assign({
          labels: ['Utils', 'Idle', 'Liters'], datasets: [],
        });
        let totalCurrentUtils = 0;
        let totalPreviousUtils = 0;
        let totalCurrentIdle = 0;
        let totalPreviousIdle = 0;
        let totalCurrentLiters = 0;
        let totalPreviousLiters = 0;

        response.data.forEach(equipment => {
          totalCurrentUtils += equipment.eqp_util_hrs;
          totalPreviousUtils += equipment.eqp_util_hrs_recent_week;
          totalCurrentIdle += equipment.eqp_idle_hrs;
          totalPreviousIdle += equipment.eqp_idle_hrs_recent_week;
          totalCurrentLiters += equipment.eqp_liters_per_hour;
          totalPreviousLiters += equipment.eqp_liters_recent_week;
        }, this);

        chartDataSet.datasets.push({
          label: 'Current',
          backgroundColor: '#10C4D3',
          data: [
            totalCurrentUtils,
            totalCurrentIdle,
            totalCurrentLiters,
          ],
        });
        chartDataSet.datasets.push({
          label: 'Previous',
          backgroundColor: '#E0CF0A',
          data: [
            totalPreviousUtils,
            totalPreviousIdle,
            totalPreviousLiters,
          ],
        });

        context.commit('setEquipmentDataSet', chartDataSet);
        context.commit('setTotalCurrentUtils', totalCurrentUtils);
        context.commit('setTotalPreviousUtils', totalPreviousUtils);
        context.commit('setTotalCurrentIdle', totalCurrentIdle);
        context.commit('setTotalPreviousIdle', totalPreviousIdle);
        context.commit('setTotalCurrentLiters', totalCurrentLiters);
        context.commit('setTotalPreviousLiters', totalPreviousLiters);
      });
  },
  fetchEquipmentType(context) {
    axios.get('/eqpGetType')
      .then((response) => {
        context.commit('setEquipmentType', response.data);
      });
  },
  fetchEquipmentModel(context) {
    axios.get('/eqpGetModel')
      .then((response) => {
        context.commit('setEquipmentModel', response.data);
      });
  },
  fetchEquipmentBrand(context) {
    axios.get('/eqpGetBrand')
      .then((response) => {
        context.commit('setEquipmentBrand', response.data);
      });
  },
  fetchDashboardChart(context) {
    axios.get('/eqpGetDashboardChart')
      .then((response) => {
        context.commit('setDashboardChart', response.data);
      });
  },
};

// ------------------------
// Store
// ------------------------

const EquipmentStore = {
  namespaced: true,
  state: EquipmentModel,
  mutations,
  getters,
  actions,
};

export default EquipmentStore;
