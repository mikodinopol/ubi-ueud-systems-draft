/**
 * LocalStorage Singleton Class
 */
class LocalStorage {
  /**
   * Class constructor.
   */
  constructor() {
    this.myStorage = window.localStorage;
  }

  /**
   * Store value by key in local storage.
   *
   * @param {string} key
   * @param {any} value The value to be stored.
  */
  set(key, value) {
    this.myStorage.setItem(key, value);
  }

  /**
   * Get value by key in local storage.
   *
   * @param {string} key
   *
   * @return {any} The value stored.
  */
  get(key) {
    return this.myStorage.getItem(key);
  }

  /**
   * Remove key from local storage.
   *
   * @param {string} key
  */
  remove(key) {
    this.myStorage.removeItem(key);
  }
}

export let Storage = new LocalStorage();
