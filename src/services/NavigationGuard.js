import { Auth } from './Auth';

import Dashboard from '@/pages/user/dashboard';
import Login from '@/pages/login';

/**
 * NavigationGuard Singleton Class
 *
 * Helper class for vue route navigation checks.
 */
class NavigationGuard {
  /**
   * User authentication check.
   *
   * @param {Vue.Route} to
   * @param {Vue.Route} from
   * @param {function} next
   */
  Authenticated(to, from, next) {
    if (!Auth.isAuthenticated()) {
      next({ name: Login.name });
    } else {
      if (to.name == 'user') {
        next({ name: Dashboard.name });
      } else {
        next();
      }
    }
  }

  /**
   * Un-authticated check.
   *
   * @param {Vue.Route} to
   * @param {Vue.Route} from
   * @param {function} next
   */
  UnAuthenticated(to, from, next) {
    if (!Auth.isAuthenticated()) {
      next();
    } else {
      next({ name: from.name });
    }
  }
}

export let NavGuard = new NavigationGuard();
