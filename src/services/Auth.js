import { Storage } from '@/services/Storage';

export const STORAGE_KEY = {
  TOKEN: 'token',
};

/**
 * Authentication Singleton Class
 */
class Authentication {
  /**
   * Check if user is authenticated.
   *
   * @return {boolean}
   */
  isAuthenticated() {
    return Storage.get(STORAGE_KEY.TOKEN) ? true : false;
  }

  /**
   * Authenticate user session by setting access token to the local storage.
   *
   * @param {string} token The access token for the API
  */
  authenticate(token) {
    Storage.set(STORAGE_KEY.TOKEN, token);
   
  }

  /**
   * Unauthenticate user session.
  */
  unAuthenticate() {
    Storage.remove(STORAGE_KEY.TOKEN);
  }
}

export let Auth = new Authentication();
