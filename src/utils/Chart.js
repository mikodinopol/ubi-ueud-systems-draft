import Colors from './Colors';

export const ChartTypes = {
  Bar: 'bar',
  Bubble: 'bubble',
  Doughnut: 'doughnut',
  Line: 'line',
  Pie: 'pie',
  PolarArea: 'polar-area',
  Radar: 'radar',
  Scatter: 'scatter',
};

/**
 * Chart.JS config class
*/
export class ChartUtil {
  /**
   * Format API data return to ChartJS compatible data format
   *
   * @param {array} data - The data from the Ulticon API (eqpGetDashboardChart)
   * @param {ChartTypes} type - Default value `Doughnut`
   *
   * @return {object} - formatted data
   */
  static formatAPIData(data, type = ChartTypes.Doughnut) {
    if (!data) return;

    const result = {
      labels: [],
      datasets: [],
    };

    switch (type) {
      // For Doughnut chart there will only be one entry for dataset
      // so we initialized it with backgroundColor and data in adv.
      case ChartTypes.Doughnut:
        result.datasets.push({
          backgroundColor: [],
          data: [],
        });
        break;
    }

    data.forEach((d) => {
      result.labels.push(d.label);

      switch (type) {
        case ChartTypes.Doughnut:
          result.datasets[0].backgroundColor.push(Colors.generateHex());
          result.datasets[0].data.push(d.value);
          break;
      }
    });

    return result;
  }
}
