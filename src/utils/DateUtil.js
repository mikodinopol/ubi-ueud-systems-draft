/**
 * Helper class for Date formatting/generation.
*/
class DateUtil {
  /**
   * Singelton constructor.
   */
  constructor() {
    if (!DateUtil.instance) {
      this._date = new Date();

      DateUtil.instance = this;
    }

    return DateUtil.instance;
  }

  /**
   * Get date format for today.
   *
   * @return {string}
   */
  getDateToday() {
    const m = this._date.getMonth();
    const d = this._date.getDate();
    const ms = (m < 10) ? `0${m}` : m;
    const ds = (d < 10) ? `0${d}` : d;

    return `${this._date.getFullYear()}-${ms}-${ds}`;
  }
  getDateTodayReport() {
    const m = this._date.getMonth() + 1;
    const d = this._date.getDate();
    const ms = (m < 10) ? `0${m}` : m;
    const ds = (d < 10) ? `0${d}` : d;

    return `${this._date.getFullYear()}-${ms}-${ds}`;
  }

  /**
   * Get a formatted date string.
   *
   * @param {date} date
   *
   * @return {string}
  */
  getDateFormat(date) {
    const m = date.getMonth();
    const d = date.getDate();
    const ms = (m < 10) ? `0${m}` : m;
    const ds = (d < 10) ? `0${d}` : d;

    return `${date.getFullYear()}-${ms}-${ds}`;
  }

  /**
   * Get a formatted date string.
   *
   * @param {date} date
   *
   * @return {string}
  */
  dayFormat(date) {
    const m = date.getMonth() + 1;
    const d = date.getDate();
    const ds = (d < 10) ? `0${d}` : d;

    return `${this.getMonth(m)} ${ds}`;
  }

  dayFormat2(date) {
    const m = date.getMonth() + 1;
    const d = date.getDate();
    const ds = (d < 10) ? `0${d}` : d;

    return `${m} ${ds}`;
  }
  getMonth(month) {
    let result = '';

    switch (month) {
      case 1:
        result = 'Jan';
        break;
      case 2:
        result = 'Feb';
        break;
      case 3:
        result = 'Mar';
        break;
      case 4:
        result = 'Apr';
        break;
      case 5:
        result = 'May';
        break;
      case 6:
        result = 'Jun';
        break;
      case 7:
        result = 'Jul';
        break;
      case 8:
        result = 'Aug';
        break;
      case 9:
        result = 'Sep';
        break;
      case 10:
        result = 'Oct';
        break;
      case 11:
        result = 'Nov';
        break;
      case 12:
        result = 'Dec';
        break;
    }

    return result;
  }
}

const instance = new DateUtil();

Object.freeze(instance);

export default instance;
