/**
 * Helper class for color/hex generation.
*/
class Colors {
  /**
   * Generate a random HEX color value.
   *
   * @return {string}
   */
  static generateHex() {
    return '#' + ('000000' + Math.random()
                                .toString(16)
                                .slice(2, 8)
                                .toUpperCase())
                                .slice(-6);
  }
}

export default Colors;
