/**
 * Helper class for formating API payload.
*/
class Payload {
  /**
   * Stringify payload object to URL query params.
   *
   * @param {object} payload The payload object to format
   *
   * @return {string}
   */
  static toURLParams(payload) {
    let result = '';

    for (let key in payload) {
      if (payload[key]) {
        if (result.length) {
          result += `&${key}=${payload[key]}`;
        } else {
          result += `${key}=${payload[key]}`;
        }
      }
    }

    return result;
  }
}

export default Payload;
