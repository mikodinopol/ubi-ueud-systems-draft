/* global process*/
/**
 * Axios service class.
 *
 * On top with axios service is to allow the intercepting of ajax calls to be able to inject authorization headers automatically.
 */

import axios from 'axios';

import { Storage } from '@/services/Storage';

import * as Auth from '@/services/Auth';

// Request Interceptor
axios.interceptors.request.use(function(config) {
  // For every request that starts with '/' it is assumed that
  // we will be requesting to the API,
  // so we append the API url on the request before initiating.
  if (config.url.indexOf('/') === 0) {
    let newUrl = `${process.env.API_URL}${config.url}`;

    // const accessToken = '572c17e31538b9e44059efe630ae6d8d';
    const accessToken = Storage.get(Auth.STORAGE_KEY.TOKEN);

    switch (config.method) {
      case 'get':
        if (newUrl.indexOf('token') === -1 && accessToken) {
          if (newUrl.indexOf('?') < 0) {
            newUrl += `?token=${accessToken}`;
          } else {
            newUrl += `&token=${accessToken}`;
          }
        }
        break;
      case 'post': {
        let params = new URLSearchParams();

        params.append('token', accessToken);

        for (let key in config.data) {
          if (config.data.hasOwnProperty(key)) {
            params.append(key, config.data[key]);
          }
        }

        config.data = params.toString();

        break;
      }
    }

    config.url = newUrl;
  }

  return config;
}, function(error) {
  // Do something with request error
  return Promise.reject(error);
});

// Response Interceptor
axios.interceptors.response.use(function(response) {
  // Do something with response data
  return response.data;
}, function(error) {
  // Do something with response error
  return Promise.reject(error.response);
});

export default axios;
