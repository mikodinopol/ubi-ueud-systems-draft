import Vue from 'vue';

Vue.filter('parseDateEpoch', (val) => {
  const date = new Date(Number(val.match(/\d/g).join('')));
  let m = date.getMonth() + 1;
      m = (m < 10) ? `0${m}` : m;
  let d = date.getDate();
      d = (d < 10) ? `0${d}` : d;

  return `${date.getFullYear()}/${ m }/${ d }`;
});
