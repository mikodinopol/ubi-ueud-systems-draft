import { Bar, DefaultChartOpts } from '../index';

export default {
  extends: Bar,
  props: ['data', 'options'],
  mounted() {
    this.renderChart(
      this.data,
      Object.assign({}, this.options || {}, DefaultChartOpts)
    );
  },
  watch: {
    data: {
      handler() {
        this.renderChart(
          this.data,
          Object.assign({}, this.options || {}, DefaultChartOpts)
        );
      },
      deep: true,
    },
  },
};
