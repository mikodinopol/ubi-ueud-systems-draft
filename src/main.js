// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import App from './App';
import router from './router';
import moment from 'moment'

import './filters';

import 'vuetify/dist/vuetify.min.css';
import VueNumeric from 'vue-numeric'
import VueCurrencyFilter from 'vue-currency-filter'


import VeeValidate from 'vee-validate';
import VueRouter from 'vue-router';

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY hh:mm')
  }
});

Vue.use(VeeValidate);

var VueCookie = require('vue-cookie');
Vue.use(VueCookie);

Vue.use(Vuetify);
Vue.use(Vuex);
Vue.use(VueNumeric)
//₱
Vue.use(VueCurrencyFilter,
  {
    symbol : '',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true
  })

Vue.config.productionTip = false;

const store = new Vuex.Store();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});